exports.addMiddleware = (express, app) => {
	app.use("/vehicle-svr", require("./mtd-vehicle-server/express-middleware.js").serveJsonFiles);
	app.use("/a3k-n/u-mta-sts/.well-known/mta-sts.txt", (req, res) => {
		res.set('Content-Type', 'text/plain');
		res.send(`version: STSv1
mode: enforce
mx: apps-vm1.peterjin.org
mx: apps-vm2.peterjin.org
mx: apps-vm1.srv.peterjin.org
mx: apps-vm2.srv.peterjin.org
max_age: 1000000
`);
	});
}
