var express = require("express");
var vehicle_static = null;
exports.serveJsonFiles = (req, res, next) => {
	if (req.path == "/") {
		res.set("Content-Type", "text/plain");
		res.set("X-Content-Type-Options", "nosniff");
		res.send(`In this folder:

reroutes.json - cached copy of getreroutes from MTD API, updated about once per hour
reroutes.pp.json - same but pretty-printed
vehicles.json - cached copy of getvehicles from MTD API, updated about every 40-45 seconds
vehicles.pp.json - same but pretty-printed

Data provided by CUMTD.
Visit https://go.peterjin.org/cumtd-api for more information.
			`);
		return;
	}
	let origin = req.get("Origin");
	const permitted_origins = ["", "https://busroutes.peterjin.org", "http://phjarea217.gitlab.io", "https://phjarea217.gitlab.io"];
	if (origin !== undefined && !(/^https?\:\/\/localhost\:?\d*$/.test(origin)) && permitted_origins.findIndex((e) => e === origin) === -1) {
		res.send(403);
		return;
	}
	vehicle_static = vehicle_static || express.static(process.env.VS_PUBLIC_DIR || "public", {etag: true, setHeaders: (res, path, stat) => {
		let p = String(path);
		if (/\/vehicles(\.pp)?\.json$/.test(p)) {
			res.set("Cache-Control", "max-age=20, private, must-revalidate");
		} else {
			res.set("Cache-Control", "max-age=900, private, must-revalidate");
		}
		res.set("X-Content-Type-Options", "nosniff");
		res.set("Vary", "Origin");
	}});
	res.set("Access-Control-Allow-Origin", origin || "");
	vehicle_static(req, res, next);
};
